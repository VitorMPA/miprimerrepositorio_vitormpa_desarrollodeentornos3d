﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Inputs : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}

    [Serializable]
    public struct InputData
    {
        public float hMovement;
        public float vMovement;

        public float verticalMouse;
        public float horizontalMouse;

        public bool dash;
        public bool jump;

        public void getInput()
        {
            hMovement = Input.GetAxis("Horizontal");
            vMovement = Input.GetAxis("Vertical");

            verticalMouse = Input.GetAxis("Mouse Y");
            horizontalMouse = Input.GetAxis("Mouse X");

            dash = Input.GetButton("Dash");
            jump = Input.GetButtonDown("Jump");
        }


    }

